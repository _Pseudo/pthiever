import com.pseudo.scripts.ikov.thiever.StallThiever;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.api.utils.Timer;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.*;
import org.rev317.min.api.wrappers.SceneObject;

/**
 * @author Corey.
 *         Created: 01/08/2014 at 17:07.
 *         Do not redistribute without approval.
 */
public class Thiever implements Strategy {

    public static SceneObject stall;

    @Override
    public boolean activate() {
        return Game.getOpenBackDialogId() != 368;
    }

    @Override
    public void execute() {
        Timer t = new Timer(2500);
        while (Players.getMyPlayer().getAnimation() != -1 && t.isRunning()) {
            Time.sleep(50);
        }
        stall = SceneObjects.getClosest(StallThiever.chosenStall.getId());
        if (stall != null) {
            if (Calculations.distanceTo(stall.getLocation()) >= 5) {
                Walking.walkTo(Players.getMyPlayer().getLocation(), stall.getLocation());
            }
            stall.interact(0);
            Time.sleep(500, 900);
        }
    }

}
