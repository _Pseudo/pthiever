import com.pseudo.scripts.ikov.thiever.StallThiever;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;

import java.awt.*;

/**
 * @author Corey.
 *         Created: 01/08/2014 at 18:23.
 *         Do not redistribute without approval.
 */
public class Relog implements Strategy {

    private final Point xButton = new Point(749, 10);
    private final Point logoutButton = new Point(640, 373);
    private final Point loginButton = new Point(387, 288);

    @Override
    public boolean activate() {
        return Game.getOpenBackDialogId() == 368 || StallThiever.loggedOut;
    }

    @Override
    public void execute() {
        if (Game.getOpenBackDialogId() == 368) {
            Mouse.getInstance().click(xButton);
            Time.sleep(500, 1000);
            Mouse.getInstance().click(logoutButton);
            Time.sleep(2500, 3500);
            StallThiever.loggedOut = true;
        }
        if (StallThiever.loggedOut) {
            for (int i = 0; i < 5; i++) {
                Mouse.getInstance().click(loginButton);
            }
            StallThiever.loggedOut = false;
        }
    }
}
