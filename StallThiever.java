import com.pseudo.scripts.ikov.thiever.worker.Relog;
import com.pseudo.scripts.ikov.thiever.worker.Thiever;
import org.parabot.environment.api.interfaces.Paintable;
import org.parabot.environment.api.utils.Timer;
import org.parabot.environment.input.Mouse;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.parabot.environment.scripts.framework.Strategy;

import java.awt.*;
import java.util.ArrayList;

/**
 * @author Corey.
 *         Created: 29/07/2014 at 01:08.
 *         Do not redistribute without approval.
 */
@ScriptManifest(name = "Stall Thiever", author = "Pseudo", description = "Thieves stalls at home on Ikov", category = Category.THIEVING, version = 0.1, servers = {"Ikov"}, vip = false, premium = false)
public class StallThiever extends Script implements Paintable {

    private final ArrayList<Strategy> nodes = new ArrayList<Strategy>();

    private Timer runTimer = new Timer(0);

    public static Stall chosenStall = Stall.FIFTH; //Change Stall.VALUE to co-respond to the stall you want to thieve.
    public static boolean loggedOut = false;

    public boolean onExecute() {
        nodes.add(new Thiever());
        nodes.add(new Relog());
        provide(nodes);
        return true;
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void paint(Graphics g) {
        drawMouse(g);
        g.drawString("pThiever", 20, 50);
        g.drawString("Stall: " + chosenStall, 20, 70);
        g.drawString("Time Running: " + format(runTimer.getElapsedTime()), 20, 90);
    }

    private void drawMouse(Graphics g) {
        g.setColor(Color.GREEN);
        final Point m = Mouse.getInstance().getPoint();
        g.drawLine(m.x - 5, m.y + 5, m.x + 5, m.y - 5);
        g.drawLine(m.x - 5, m.y - 5, m.x + 5, m.y + 5);
    }

    public static String format(final long time) {
        int sec = (int) (time / 1000), h = sec / 3600, m = sec / 60 % 60, s = sec % 60;
        return (h < 10 ? "0" + h : h) + ":" + (m < 10 ? "0" + m : m) + ":" + (s < 10 ? "0" + s : s);
    }

    public enum Stall {
        FIRST(1, 4875),
        SECOND(30, 4874),
        THIRD(60, 4876),
        FOURTH(65, 4877),
        FIFTH(80, 4878);

        private final int level, id;

        private Stall(final int level, final int id) {
            this.level = level;
            this.id = id;
        }

        public int getLevel() {
            return level;
        }

        public int getId() {
            return id;
        }
    }
}
